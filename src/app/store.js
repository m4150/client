import { configureStore } from "@reduxjs/toolkit";
import typesReducer from '../features/TransactionTypes/slice';
import transactionsReducer from '../features/Transactions/slice';
import sheetsReducer from '../features/Sheets/slice';
import appReducer from '../features/App/slice';

export default configureStore({
    reducer: {
        types: typesReducer,
        transactions: transactionsReducer,
        sheets: sheetsReducer,
        app:  appReducer
    },
    devTools: true
})