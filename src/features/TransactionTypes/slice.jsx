import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const initialState = {
  allTypes: [],
  inflowCategories: [],
  outflowCategories: [],
};

export const getTypes = createAsyncThunk("types/getTypes", async () => {
  let list;
  await axios
    .get("http://localhost:5000/types", { username: 'Madusara' })
    .then((res) => (list = res.data))
    .catch((err) => console.log(err));
  return list;
});

export const getInflowCategories = createAsyncThunk("types/getInflowCategories", async (id) => {
  let list;
  await axios
    .get("http://localhost:5000/types/inflows", { username: 'Madusara' })
    .then((res) => (list = res.data))
    .catch((err) => console.log(err));
  return list;
});

export const getOutflowCategories = createAsyncThunk("types/getOutflowCategories", async () => {
  let list;
  await axios
    .get("http://localhost:5000/types/outflows", { username: 'Madusara' })
    .then((res) => (list = res.data))
    .catch((err) => console.log(err));
  return list;
});

export const deleteItem = createAsyncThunk("types/deleteType", async (data) => {
  let list;
  await axios
    .put(`http://localhost:5000/types/delete/${data.id}`, data)
    .then((res) => (list = res.data))
    .catch((err) => console.log(err));
  return list;
});

export const addCategory = createAsyncThunk(
  "types/addCategory",
  async (data) => {
    let list;
    await axios
      .post("http://localhost:5000/types/add", {
        userID: data.userID,
        name: data.title,
        isExpense: data.isExpense,
      })
      .then((res) => (list = res.data))
      .catch((err) => console.log(err, data));
    return list;
  }
  // );
);

const typesSlice = createSlice({
  name: "types",
  initialState,
  extraReducers: {
    [getTypes.pending]: (state, action) => {},
    [getTypes.fulfilled]: (state, action) => {
      state.allTypes = action.payload;
    },
    [getInflowCategories.fulfilled]: (state, action) => {
      state.inflowCategories = action.payload;
    },
    [getOutflowCategories.fulfilled]: (state, action) => {
      state.outflowCategories = action.payload;
    },
    [deleteItem.fulfilled]: (state, action) => {
      state.allTypes = state.allTypes.filter(
        (type) => type._id !== action.payload._id
      );
    },
    [addCategory.fulfilled]: (state, action) => {
      state.allTypes = action.payload;
    },
  },
});

export const { updateAllTypes } = typesSlice.actions;
export default typesSlice.reducer;
