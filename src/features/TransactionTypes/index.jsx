import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  getTypes,
  getInflowCategories,
  getOutflowCategories,
  deleteItem,
  addCategory,
} from "./slice";
import trash from "../../images/trash.png";
import "./index.scss";

const TransactionTypes = () => {
  const [incomeTitle, setIncomeTitle] = useState("");
  const [expensesTitle, setExpensesTitle] = useState("");
  // const [allCategoriesList, setAllCategoriesList] = useState([]);
  const dispatch = useDispatch();
  let initList = useSelector((state) => state.types.allTypes);
  const myID = '61e8227189878800650ac4db';
  const sheetID = useSelector((state) => state.app.userData.activeSheet);
  // useEffect(() => {
  //   if (initList.length === 0) {
  //     dispatch(addCategory({ title: "Any", isExpense: false }));
  //     dispatch(addCategory({ title: "Any", isExpense: true }));
  //   }
  // }, [initList]);

  useEffect(() => {
    dispatch(getTypes());
    dispatch(getInflowCategories(myID));
    dispatch(getOutflowCategories());
  }, [dispatch]);

  const onDeleteItem = (id) => {
    dispatch(deleteItem({ userID: myID, sheetID: sheetID, id: id }));
  };

  const incomeCategoriesList = () => {
    return initList
      .filter((category) => !category.isExpense)
      .map((category, index) => (
        <div className="eachIncomeCategory" key={index}>
          {category.name}{" "}
          {!category.isDefault && (
            <div className="deleteBtn" onClick={() => onDeleteItem(category._id)}>
              <img src={trash} alt="" />
            </div>
          )}
        </div>
      ));
  };

  const expensesCategoriesList = () => {
    return initList
      .filter((category) => category.isExpense)
      .map((category, index) => (
        <div className="eachExpenseCategory" key={index}>
          {category.name}{" "}
          {!category.isDefault && (
            <div className="deleteBtn" onClick={() => onDeleteItem(category._id)}>
              <img src={trash} alt="" />
            </div>
          )}
        </div>
      ));
  };

  const addIncomeKeyUp = (e) => {
    if (e.keyCode === 13) {
      dispatch(addCategory({ userID: myID, title: incomeTitle, isExpense: false }));
      setIncomeTitle("");
    }
  };

  const addExpenseKeyUp = (e) => {
    if (e.keyCode === 13) {
      dispatch(addCategory({ userID: myID, title: expensesTitle, isExpense: true }));
      setExpensesTitle("");
    }
  };

  return (
    <div className="TRANSACTION_TYPES--">
      <div className="inner">
        <div className="categories">
          <div className="income">
            <h4>Income categories</h4>
            <div className="list">{incomeCategoriesList()}</div>
          </div>
          <div className="expenses">
            <h4>Expense categories</h4>
            <div className="list">{expensesCategoriesList()}</div>
          </div>
        </div>
        <div className="addCategoryRow">
          <input
            type="text"
            placeholder="Add +"
            value={incomeTitle}
            onChange={(e) => setIncomeTitle(e.target.value)}
            onKeyUp={(e) => addIncomeKeyUp(e)}
          />
          <input
            type="text"
            placeholder="Add +"
            value={expensesTitle}
            onChange={(e) => setExpensesTitle(e.target.value)}
            onKeyUp={(e) => addExpenseKeyUp(e)}
          />
        </div>
      </div>
    </div>
  );
};

export default TransactionTypes;
