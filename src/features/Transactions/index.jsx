import React, { useState, useEffect } from "react";
import "./index.scss";
import EachTransaction from "../EachTransaction";
import enter from "../../images/enter.png";
import {
  getSheets,
  fetchAllTransactions,
  submitInflow,
  submitOutflow,
  deleteTransaction,
  addSheet
} from "./slice";
import {
  getTypes
} from "../TransactionTypes/slice";
// import {
//   getSheets
// } from "../Sheets/slice";
import {
  activateSheet
} from "../App/slice";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { useDispatch, useSelector } from "react-redux";

const Transactions = () => {
  const [sheetName, setSheetName] = useState("");
  const [incomeTransactionName, setIncomeTransactionName] = useState("");
  const [incomeTransactionValue, setIncomeTransactionValue] = useState("");
  const [expenseTransactionName, setExpenseTransactionName] = useState("");
  const [expenseTransactionValue, setExpenseTransactionValue] = useState("");
  const [sheetSelectPopupOpen, setsheetSelectPopupOpen] = useState(false);
  const inflowsList = useSelector((state) => state.transactions.allTransactions.filter(transaction => !transaction.isExpense));
  const outflowsList = useSelector((state) => state.transactions.allTransactions.filter(transaction => transaction.isExpense));
  const [inflowCategory, setInflowCategory] = useState({});
  const [outflowCategory, setOutflowCategory] = useState({});
  const [expenseCategoryPopupOpen, setExpenseCategoryPopupOpen] =
    useState(false);
  const [incomeCategoryPopupOpen, setIncomeCategoryPopupOpen] = useState(false);
  const expenseTypes = useSelector((state) => state.types.allTypes.filter(type => type.isExpense));
  const incomeTypes = useSelector((state) => state.types.allTypes.filter(type => !type.isExpense));
  const sheetsList = useSelector((state) => state.transactions.sheets);
  const activeSheet = useSelector((state) => state.app.userData.activeSheet);
  const myData = useSelector((state) => state.app.userData);

  const dispatch = useDispatch();

  const toggleExpenseCategoryPopup = () =>
    setExpenseCategoryPopupOpen((prevState) => !prevState);
  const toggleIncomeCategoryPopup = () =>
    setIncomeCategoryPopupOpen((prevState) => !prevState);

  useEffect(() => {
    dispatch(fetchAllTransactions());
    dispatch(getTypes());
  }, [dispatch]);

  useEffect(() => {
    console.log("localala", sheetsList)
  }, [sheetsList]);

  const submitIncomeTransaction = (e) => {
    e.preventDefault();
    const data = {
      name: incomeTransactionName,
      value: parseInt(incomeTransactionValue),
      transactionType: inflowCategory ? inflowCategory._id : '',
      isExpense: false,
    };
    dispatch(submitInflow(data));
    setInflowCategory({});
  };

  const submitExpenseTransaction = (e) => {
    e.preventDefault();
    const data = {
      name: expenseTransactionName,
      value: parseInt(expenseTransactionValue),
      transactionType: outflowCategory._id,
      isExpense: true,
    };
    dispatch(submitOutflow(data));
  };

  const toggleSheetSelectPopup = () => {
    setsheetSelectPopupOpen(!sheetSelectPopupOpen);
  }

  const onSheetNameChange = (e) => {
    setSheetName(e.target.value)
  }

  const handleSheetSelect = (sheet) => {
    const data = {
      userID: myData._id,
      sheetID: sheet._id
    }
    dispatch(activateSheet(data))
  }

  const sheetNameKeyDown = (e) => {
    if(e.keyCode === 13){
      e.preventDefault();
      dispatch(addSheet({
        name: sheetName,
        userID: myData._id
      }))
      setSheetName('')
    }
  }

  const onDeleteTransaction = (id) => {
    dispatch(deleteTransaction(id))
  };

  const balance = () => {
    let count = 0;
    inflowsList.forEach((transaction) => {
      count += transaction.value;
    });
    outflowsList.forEach((transaction) => {
      count -= transaction.value;
    });
    return count;
  };

  const activeSheetName = () => {
    if(activeSheet) {
      const result = myData.sheets && activeSheet && myData.sheets.filter(sheet => sheet._id === activeSheet);
      return result[0].name;
    } else return '';
  }

  return (
    <div className="TRANSACTIONS--">
      <div className="componentHeader">
        <div>
          <Dropdown
            className="sheetSelect"
            isOpen={sheetSelectPopupOpen}
            toggle={toggleSheetSelectPopup}
          >
            <DropdownToggle>
              {activeSheet && Object.keys(activeSheet).length === 0 ? 'Please Select' : activeSheetName()}
            </DropdownToggle>
            <DropdownMenu>
              {sheetsList && sheetsList.length > 0 &&
                sheetsList[0].map((sheet, index) => {
                  return (
                    <DropdownItem
                      key={index}
                      onClick={() => handleSheetSelect(sheet)}
                    >
                      {sheet.name}
                    </DropdownItem>
                  );
                })}
            </DropdownMenu>
          </Dropdown>
          <input placeholder="Sheet name" onChange={e => onSheetNameChange(e)} value={sheetName} onKeyDown={e => sheetNameKeyDown(e)} />
        </div>
        <h4 className="balance">{balance()}</h4>
      </div>
      <div className="allTransactions">
        <div className="incomeSection">
          {inflowsList.map((transaction, index) => {
            return (
              <EachTransaction
                key={index}
                transaction={transaction}
                deleteTransaction={onDeleteTransaction}
              />
            );
          })}
          <>
            <h4 className="date">10/09/2021</h4>
            <input
              type="text"
              onChange={(e) => setIncomeTransactionName(e.target.value)}
              value={incomeTransactionName}
              placeholder="Eg: Misc earnings"
            />
            <input
              type="number"
              onChange={(e) => setIncomeTransactionValue(e.target.value)}
              value={incomeTransactionValue}
              placeholder="Eg: 10000"
            />
            <Dropdown
              className="categoryPopup"
              isOpen={incomeCategoryPopupOpen}
              toggle={toggleIncomeCategoryPopup}
            >
              <DropdownToggle>
                {Object.keys(inflowCategory).length > 0 ? inflowCategory.name : "Select"}
              </DropdownToggle>
              <DropdownMenu>
                {incomeTypes.length > 0 &&
                  incomeTypes.map((incomeType, index) => {
                    return (
                      <DropdownItem
                        key={index}
                        onClick={() => setInflowCategory(incomeType)}
                      >
                        {incomeType.name}
                      </DropdownItem>
                    );
                  })}
              </DropdownMenu>
            </Dropdown>
            <button
              className="enterTransaction"
              onClick={(e) => submitIncomeTransaction(e)}
            >
              <img src={enter} alt="" />
            </button>
          </>
        </div>
        <div className="expensesSection">
          {outflowsList.map((transaction, index) => {
            return (
              <EachTransaction
                key={index}
                transaction={transaction}
                deleteTransaction={onDeleteTransaction}
                expenseTypes
                incomeTypes
              />
            );
          })}
          <>
            <h4 className="date">10/09/2021</h4>
            <input
              type="text"
              onChange={(e) => setExpenseTransactionName(e.target.value)}
              value={expenseTransactionName}
              placeholder="Eg: Train ticket"
            />
            <input
              type="number"
              onChange={(e) => setExpenseTransactionValue(e.target.value)}
              value={expenseTransactionValue}
              placeholder="Eg: 120"
            />
            <Dropdown
              className="categoryPopup"
              isOpen={expenseCategoryPopupOpen}
              toggle={toggleExpenseCategoryPopup}
            >
              <DropdownToggle>
                {Object.keys(outflowCategory) > 0 ? outflowCategory.name : "Select"}
              </DropdownToggle>
              <DropdownMenu>
                {expenseTypes.length > 0 &&
                  expenseTypes.map((expenseType, index) => {
                    return (
                      <DropdownItem
                        key={index}
                        onClick={() => setOutflowCategory(expenseType)}
                      >
                        {expenseType.name}
                      </DropdownItem>
                    );
                  })}
              </DropdownMenu>
            </Dropdown>
            <button
              className="enterTransaction"
              onClick={(e) => submitExpenseTransaction(e)}
            >
              <img src={enter} alt="" />
            </button>
          </>
        </div>
      </div>
    </div>
  );
};


export default Transactions;