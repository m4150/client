import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const initialState = {
  allTransactions: [],
  sheets: []
};

export const fetchAllTransactions = createAsyncThunk(
  "transactions/fetchAllTransactions",
  async () => {
    let list;
    await axios
      .get("http://localhost:5000/transactions/")
      .then(res =>  list = res.data)
      .catch((err) => console.log(err));
    return list;
  }
);

export const addSheet = createAsyncThunk(
  "transactions/addSheet",
  async (data) => {
    let list;
    await axios
      .post("http://localhost:5000/sheets/add", data)
      .then(resp => {
        list = resp.data
      })
      .catch((err) => console.log(err));
    return list;
  }
);

export const getSheets = createAsyncThunk("sheets/getSheets", async (username) => {
  let list;
  console.log("param", username)
  await axios
    .get("http://localhost:5000/sheets", {
      params: {
        username: username
      }
    })
    .then((res) => {
      console.log(res.data,"goro")
      list = res.data
    })
    .catch((err) => console.log(err));
  return list;
});

export const submitInflow = createAsyncThunk(
  "transactions/submitInflow",
  async (data) => {
    let list;
    await axios
      .post("http://localhost:5000/transactions/add", data)
      .then(res => list = res.data)
      .catch((err) => console.log(err));
    return list;
  }
);

export const submitOutflow = createAsyncThunk(
  "transactions/submitOutflow",
  async (data) => {
    let list;
    await axios
      .post("http://localhost:5000/transactions/add", data)
      .then(res => list = res.data)
      .catch((err) => console.log(err));
    return list;
  }
);

export const modifyTransaction = createAsyncThunk(
  "transactions/modifyTransaction",
  async (data) => {
    let list;
    await axios
      .put("http://localhost:5000/transactions/modify", data)
      .then(res => list = res.data)
      .catch((err) => console.log(err));
    return list;
  }
);

export const deleteTransaction = createAsyncThunk(
  "transactions/deleteTransaction",
  async (id) => {
    let result;
    await axios
    .put("http://localhost:5000/transactions/delete", {id: id})
      .then(res => result = id)
      .catch((err) => console.log(err));
    return result;
  }
);

const transactionsSlice = createSlice({
  name: 'transactions',
  initialState,
  extraReducers: {
    [addSheet.fulfilled]: (state, action) => {
      state.sheets = [...state.sheets, action.payload];
    },
    [getSheets.fulfilled]: (state, action) => {
      state.sheets = [action.payload];
    },
    [submitInflow.fulfilled]: (state, action) => {
      state.allTransactions = [...state.allTransactions, action.payload];
    },
    [submitOutflow.fulfilled]: (state, action) => {
      state.allTransactions = [...state.allTransactions, action.payload];
    },
    [fetchAllTransactions.fulfilled]: (state, action) => {
      state.allTransactions = action.payload
    },
    [modifyTransaction.fulfilled]: (state, action) => {
      state.allTransactions = [...state.allTransactions.filter(transaction => transaction._id !== action.payload._id), action.payload];
    },
    [deleteTransaction.fulfilled]: (state, action) => {
      state.allTransactions = state.allTransactions.filter(transaction => transaction._id !== action.payload);
    },
  }
})

export default transactionsSlice.reducer;