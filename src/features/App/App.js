import {useEffect} from 'react'
import './App.scss';
// import { AddTransaction } from './features/AddTransaction';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import TransactionTypes from '../TransactionTypes';
import Transactions from '../Transactions';
import Sheets from '../Sheets'
import { useDispatch, useSelector } from 'react-redux';
import { getUser } from './slice';
import {
  getSheets
} from "../Transactions/slice";

function App() {
  const userData = useSelector(state => state.app.userData)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUser('Madusara'));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getSheets('Madusara'))
  }, [dispatch]);
  

  return (
    <div className="App">
      <Router>
        <div className="topNavbar">
          <Link to="/transactions">Transactions</Link>
          <Link to="/sheets">Sheets</Link>
          <Link to="/transaction-types">Categories</Link>
          <div>{userData?.name}</div>
        </div>
        <div className="content">
          <Switch>
            <Route path="/transactions">
              <Transactions />
            </Route>
            <Route path="/transaction-types">
              <TransactionTypes />
            </Route>
            <Route path="/sheets">
              <Sheets />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
