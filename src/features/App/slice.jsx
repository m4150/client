import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const initialState = {
  userData: {}
};

export const getUser = createAsyncThunk("app/getUser", async () => {
    let user;
    await axios
      .get("http://localhost:5000/user", {
        params: {
          username: 'Madusara',
        },
      })
      .then((res) => {
        user = res.data
      })
      .catch((err) => console.log("12231",err));
    return user;
});

export const activateSheet = createAsyncThunk("app/activateSheet", async (data) => {
  let sheet;
  await axios
    .post("http://localhost:5000/sheets/activate", {
      userID: data.userID,
      sheetID: data.sheetID
    })
    .then((res) => {
      sheet = res.data
      console.log('receiveddd', res.data)
    })
    .catch((err) => console.log(err));
  return sheet;
});

const appSlice = createSlice({
    name: 'app',
    initialState,
    extraReducers: {
      [getUser.fulfilled]: (state, action) => {
        state.userData = action.payload;
      },
      [activateSheet.fulfilled]: (state, action) => {
        state.userData = action.payload;
      },
    }
  })
  
  export default appSlice.reducer;