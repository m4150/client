import React from 'react'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Modal } from 'reactstrap';

const EditTransactionModal = () => {
    return (
        <Modal>
            <div>
                <input type="text" />
                <input type="number" />
                <Dropdown
                    className="categoryPopup"
                    isOpen={incomeCategoryPopupOpen}
                    toggle={toggleIncomeCategoryPopup}
                >
                    <DropdownToggle>{inflowCategory.name ? inflowCategory.name : 'Select'}</DropdownToggle>
                    <DropdownMenu>
                        {incomeTypes.length > 0 &&
                        incomeTypes.map((incomeType) => {
                            return <DropdownItem onClick={() => setInflowCategory(incomeType)}>{incomeType.name}</DropdownItem>;
                        })}
                    </DropdownMenu>
                </Dropdown>
            </div>
        </Modal>
    )
}

export default EditTransactionModal
