import React, { useState, useEffect, useRef } from "react";
import moment from "moment";
import { useSelector, useDispatch  } from "react-redux";

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Modal,
} from "reactstrap";
import pencil from "../../images/pencil.png";
import trash from "../../images/trash.png";
import downArrow from '../../images/down-arrow.svg';
import { getInflowCategories, getOutflowCategories } from '../TransactionTypes/slice';
import { modifyTransaction } from '../Transactions/slice';

import "./index.scss";

const EachTransaction = ({ transaction, deleteTransaction }) => {
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [categoryPopupOpen, setCategoryPopupOpen] = useState(false);
  const inflowCategoriesList = useSelector(state =>  state.types.inflowCategories)
  const outflowCategoriesList = useSelector(state =>  state.types.outflowCategories)
  const [transactionName, setTransactionName] = useState(transaction.name);
  const [transactionVal, setTransactionVal] = useState(transaction.value);
  const [transactionIsExpense, setTransactionIsExpense] = useState(
    transaction.isExpense
  );
  const [transactionCategory, setTransactionCategory] = useState(
    transaction.category
  );
  const initialRender = useRef(true);
  const toggleEditModal = () => setEditModalOpen((prevState) => !prevState);
  const toggleCategoryPopup = () =>
    setCategoryPopupOpen((prevState) => !prevState);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getInflowCategories());
    dispatch(getOutflowCategories());
  }, [dispatch])

  const categoryDropdownList = () => {
    if (transactionIsExpense) {
      return outflowCategoriesList.map((expenseType, index) => (
        <DropdownItem onClick={() => setTransactionCategory(expenseType)}>
          {expenseType.name}
        </DropdownItem>
      ));
    } else {
      return inflowCategoriesList.map((incomeType, index) => (
        <DropdownItem onClick={() => setTransactionCategory(incomeType)}>
          {incomeType.name}
        </DropdownItem>
      ));
    }

    // incomeTypes.length > 0 &&
    //               incomeTypes.map((incomeType) => {
    //                 return <DropdownItem onClick={() => setInflowCategory(incomeType)}>{incomeType.name}</DropdownItem>;
    //               })
  };

  const saveChanges = (e) => {
    e.preventDefault();
    const data = {
      id: transaction._id,
      name: transactionName,
      value: parseInt(transactionVal),
      transactionType: transactionCategory._id,
      isExpense: transactionIsExpense,
    };
    dispatch(modifyTransaction(data))
  };

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false;
    } else setTransactionCategory("");
  }, [transactionIsExpense]);

  useEffect(() => {
  }, [transactionCategory, transactionName, transactionVal]);

if(transaction == null) return null;
else return (
    <>
      <h4 className="date">{moment(transaction.time).format("MM/DD/YYYY")}</h4>
      <h3 className="title">{transaction && transaction.name}</h3>
      <h4 className="amount">{transaction.value}</h4>
      <div className="categoryLabel">{transaction && transaction.category ? transaction.category.name : 'Any'}</div>
      <div className="editAndDeleteButtons">
        <button onClick={toggleEditModal}>
          <img src={pencil} alt="" />
        </button>
        <button onClick={() => deleteTransaction(transaction._id)}>
          <img src={trash} alt="" />
        </button>
      </div>
      {/* <Dropdown
        className="morePopup"
        isOpen={morePopupOpen}
        toggle={toggleMorePopup}
      >
        <DropdownToggle>
          <img src={ellipsis} alt="" />
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem>
            <button onClick={toggleEditModal}>Update</button>
          </DropdownItem>
          <DropdownItem>
            <button onClick={() => deleteTransaction(transaction._id)}>
              Delete
            </button>
          </DropdownItem>
        </DropdownMenu>
      </Dropdown> */}
      <Modal
        isOpen={editModalOpen}
        toggle={toggleEditModal}
        className="editTransactionModal"
      >
        <div className="modalTitle">Edit transaction</div>
        <div className="modalBody">
          <div className="field">
            <div className="label">Title</div>
            <input
              type="text"
              value={transactionName}
              onChange={(e) => setTransactionName(e.target.value)}
            />
          </div>
          <div className="field">
            <div className="label">Amount</div>
            <input
              type="text"
              value={transactionVal}
              onChange={(e) => setTransactionVal(e.target.value)}
            />
          </div>
          <div className="field">
            <div className="label">Type</div>
            <div className="toggleButtons">
              <button className={transactionIsExpense ? '' : 'highlight'} onClick={() => setTransactionIsExpense(false)}>
                Income
              </button>
              <button className={transactionIsExpense ? 'highlight' : ''} onClick={() => setTransactionIsExpense(true)}>
                Expense
              </button>
            </div>
          </div>
          <div className="field">
            <div className="label">Category</div>
            <div className="dropdownWrapper">
              <Dropdown
                className="categoryPopup"
                isOpen={categoryPopupOpen}
                toggle={toggleCategoryPopup}
                caret
              >
                <DropdownToggle>
                  {transactionCategory && transactionCategory.name
                    ? transactionCategory.name
                    : "Select"}
                  <img src={downArrow} alt="" />
                </DropdownToggle>
                <DropdownMenu>{categoryDropdownList()}</DropdownMenu>
              </Dropdown>
            </div>
          </div>
          <div className="submit">
            <button onClick={toggleEditModal}>Cancel</button>
            <button onClick={saveChanges}>Save changes</button>
          </div>
        </div>
      </Modal>
    </>
  )
};

export default EachTransaction;
