import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { activateSheet } from '../App/slice'
import "./index.scss";

const Sheets = () => {
  const dispatch = useDispatch();
  const sheetsList = useSelector((state) => state.transactions.sheets);
  

  const makeActiveSheet = (id) => {
    dispatch(activateSheet(id))
  }

  return (
    <div className="SHEETS--">
      <div className="inner">
        <h1>Sheets</h1>
        <ul>
          {sheetsList && sheetsList[0].map(sheet => {
            return <li onClick={() => makeActiveSheet(sheet._id)}>{sheet.name}</li>
          })}
        </ul>
      </div>
    </div>
  );
};

export default Sheets;
