import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const initialState = {
  activeSheet: {}
};

// export const getSheets = createAsyncThunk("sheets/getSheets", async (userID) => {
//   let list;
//   console.log("param", userID)
//   await axios
//     .get("http://localhost:5000/sheets", {
//       params: {
//         userID
//       }
//     })
//     .then((res) => {
//       list = res.data.sheets
//     })
//     .catch((err) => console.log(err));
//   return list;
// });

const sheetsSlice = createSlice({
  name: "sheets",
  initialState,
  extraReducers: {
    // [getSheets.fulfilled]: (state, action) => {
    //   state.allSheets = action.payload;
    // },
  },
});

export default sheetsSlice.reducer;
